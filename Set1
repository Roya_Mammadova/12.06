--Set 1

--DDL operations**: Create a copy of the "employees" table named "employees_backup" without including the data.
CREATE TABLE employees_backup (
    employee_id    NUMBER,
    first_name     VARCHAR2(20),
    last_name      VARCHAR2(25),
    email          VARCHAR2(25),
    phone_number   VARCHAR(20),
    hire_date      DATE,
    job_id         VARCHAR2(10),
    salary         NUMBER,
    commission_pct NUMBER,
    manager_id     NUMBER,
    department_id  NUMBER
);

--DML operations (UPDATE)**: Update the salary of all employees in the "employees_backup" table
--by adding a 15% raise for those who were hired in the year 2000.
UPDATE employees_backup
SET
    salary = salary * 1.15
WHERE
    EXTRACT(YEAR FROM hire_date) = 2000;

--Single Function (Date)**: List all employees in the "employees" table who were hired on a Friday.
SELECT
    *
FROM
    employees
WHERE
    to_char(hire_date, 'D') = 5;

--Aggregate Functions**: Calculate the total salary paid per job in the "employees" table.
SELECT
    SUM(salary),
    job_id
FROM
    employees
GROUP BY
    job_id;

--Joins**: Using a LEFT JOIN, list all departments from the "departments" table
--and the count of employees in each department from the "employees" table.
SELECT
    COUNT(employee_id),
    department_name
FROM
    employees   e
    LEFT JOIN departments d ON e.department_id = d.department_id
GROUP BY
    department_name;
